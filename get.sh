#!/usr/bin/env bash

git submodule init

git submodule  add  https://github.com/ookk33/PowerShell.git

git submodule add https://github.com/ookk33/pupy.git

git submodule add https://github.com/ookk33/xiki.git

git submodule add https://github.com/ookk33/gitsome.git

git submodule add https://github.com/ookk33/jquery.terminal.git

git submodule add https://github.com/ookk33/WebShell.git

git submodule add https://github.com/ookk33/xonsh.git

git submodule add https://github.com/ookk33/fish-shell.git

git submodule add https://github.com/ookk33/fish-shell-setup-osx.git

git submodule add https://github.com/ookk33/dotfiles.git

git submodule add https://github.com/ookk33/dotfiles-1.git dotifles-b


git submodule update